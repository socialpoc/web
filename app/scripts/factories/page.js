'use strict';

/**
 * @ngdoc function
 * @name webApp.factory.PageFactory
 * @description
 * # PageFactory
 * Factory of the webApp
 */
angular.module('webApp')
  .factory('PageFactory', function () {
    let title = 'socialPoc';
    let description = 'This is a social POC web app';
    let authenticated = false;
    let messages= {
      successes: [],
      warnings: [],
      errors: []
    };
    return {
      isAuthenticated: function () {
        return authenticated;
      },
      getSuccesses: function () {
        return messages.successes;
      },
      getWarnings: function () {
        return messages.warnings;
      },
      flushAll: function() {
        messages.successes = [];
        messages.warnings = [];
        messages.errors = [];
      },
      flush: function(what, idx) {
        console.log('flush %s %s', what, idx);
        messages[what].splice(idx, 1);
        console.log(messages);
      },
      getErrors: function () {
        return messages.errors;
      },
      getDescription: function () {
        return description;
      },
      getTitle: function () {
        return title;
      },
      setAuthenticated: function (newAuthenticated) {
        authenticated = newAuthenticated;
      },
      setDescription: function (newDescription) {
        description = newDescription;
      },
      addWarning: function (message, data) {
        this.addMessage('warnings', message,  data);
      },
      addError: function (message, err) {
        this.addMessage('errors', message, err);
      },
      addMessage: function (what, message, data) {
        let mess = message;
        if (!!data && !!data.statusText) {
          mess += ' (' + data.statusText + ')';
        }
        console.log('Page.%s: %s', what, mess);
        // assume message is not already in array to prevent ngRepeat:dupes] Duplicates in a repeater are not allowed
        // TODO: manage ng-repeat with more than one same message string
        if (messages[what].indexOf(mess) === -1) {
          messages[what].push(mess);
        }
        console.log(messages);
      },
      addSuccess: function (message, data) {
        this.addMessage('successes', message,  data);
      },
      setTitle: function (newTitle) {
        title = newTitle;
      }
    };
  });
