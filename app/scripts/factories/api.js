'use strict';

/**
 * @ngdoc function
 * @name webApp.factory.ApiFactory
 * @description
 * # ApiFactory
 * Factory of the webApp
 */
angular.module('webApp')
  .factory('ApiFactory', ['$http', 'apiUrl', function ($http, apiUrl) {
    let token = localStorage.getItem('userToken');

    function getType(type) {
      switch (type) {
        case 'people':
        case 'peoples':
          return 'people';
        case 'wall':
        case 'walls':
          return 'walls';
        case 'message':
        case 'messages':
          return 'messages';
        default:
          return null;
      }
    }

    function getConfig(type, id) {
      let config = {
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      };
      if (!!id) {
        config.url = apiUrl+'/'+getType(type)+'/'+id;
      } else {
        config.url = apiUrl+'/'+getType(type);
      }
      return config;
    }

    function getIriFrom(type, item) {
      if (!!item.id) {
        return '/api/' + getType(type) + '/' + item.id;
      }
      return '/api/' + getType(type) + '/' + item;
    }

    return {
      getIrisFrom: function (type, data) {
        let arr = [];
        angular.forEach(data, function (item) {
          arr.push(getIriFrom(type, item));
        });
        return arr;
      },
      getIriFrom: getIriFrom,
      save: function (type, data, id) {
        let config = getConfig(type, id);
        if (!!id) {
          config.method = 'PUT';
        } else {
          config.method = 'POST';
        }
        config.data = data;
        console.log('api sending %s %s', config.method, config.url);
        console.log(data);
        return new Promise(function (resolve, reject) {
          $http(config)
            .then(function (data) {
              console.log('api %s %s success', config.method, config.url);
              console.log(data);
              resolve(data.data);
            }, function (err) {
              console.log('api %s %s error', config.method, config.url);
              console.log(err);
              reject(err);
            });
        });

      },
      load: function (type, id) {
        let config = getConfig(type, id);
        config.method = 'GET';
        return new Promise(function (resolve, reject) {
          $http(config)
            .then(function (data) {
              console.log('api %s %s success', config.method, config.url);
              console.log(data);
              resolve(data.data);
            }, function (err) {
              console.log('api %s %s error', config.method, config.url);
              console.log(err);
              reject(err);
            });
        });

      }
    };

  }])
;
