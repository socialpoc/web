'use strict';

/**
 * @ngdoc function
 * @name webApp.factory.AuthenticationFactory
 * @description
 * # AuthenticationFactory
 * Factory of the webApp
 */
angular.module('webApp')
.factory('AuthenticationFactory', ['$http', 'UtilsFactory', 'apiUrl', function($http, utils, apiUrl) {

  let prototype = {};
  prototype.user = {};

  function saveToken(data) {
    console.log('saveToken');
    console.log(data);
    if (!!data.token) {
      prototype.user.token = data.token;
      localStorage.setItem('userToken', data.token);
    }
  }

  function saveUser(data) {
    console.log('saveUser');
    console.log(data);
    if ((!!data.id) && (!!data.username)) {
      prototype.user.id = data.id;
      prototype.user.login = data.username;
      localStorage.setItem('userId', data.id);
      localStorage.setItem('userLogin', data.username);
    }
  }

  function loadUserByToken() {
    return $http({
      url: apiUrl + '/me',
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + prototype.user.token
      }
    });
  }

  function loadUserById() {
    return $http({
      url: apiUrl + '/people/' + prototype.user.id,
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + prototype.user.token
      }
    });
  }

  function loadToken(login, password) {
    return $http({
      url: apiUrl+'/login_check',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      data: {
        'username': login,
        'password': password
      }
    });
  }

  function createUser(login, password) {
    return $http({
      url: apiUrl+'/people',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      data: {
        'username': login,
        'password': password
      }
    });
  }

  function login(login, password) {
    console.log('login:%s, pass:%s', login, password);
    return new Promise(function (resolve, reject) {
      loadToken(login, password)
        .then(function (data) {
          saveToken(data.data);
          loadUserByToken(data)
            .then(function (data) {
              console.log('loadUserByToken success');
              saveUser(data.data);
              console.log(data);
              resolve(data.data);
            }, function (err) {
              console.log('loadUserByToken error');
              console.log(err);
              reject(err);
            });
        }, function (err) {
          if ((!!err.status) && 401 === err.status) {
            console.log(err.message);
            createUser(login, password)
              .then(function (data) {
                saveUser(data.data);
                loadToken(login, password)
                  .then(function(data) {
                    saveToken(data.data);
                    resolve(data.data);
                  }, function(err) {
                    console.log('loadToken failed twice (after createUser too !!!)');
                    console.log(err);
                    reject(err);
                  });
              }, function (err) {
                console.log('createUser failed');
                console.log(err);
                reject(err);
              });
          } else {
            console.log('loadToken failed');
            console.log(err);
            reject(err);
          }
        });
    });
  }

  function logout() {
    return new Promise(function (resolve) {
        prototype.user = {};
        localStorage.removeItem('userToken');
        localStorage.removeItem('userId');
        localStorage.removeItem('userLogin');
        resolve();
    });
  }

  function loadCurrent() {
    prototype.user.id = localStorage.getItem('userId');
    prototype.user.login = localStorage.getItem('userLogin');
    prototype.user.token = localStorage.getItem('userToken');

    let promise;
    if (utils.isBlankString(prototype.user.id) || utils.isBlankString(prototype.user.token) || utils.isBlankString(prototype.user.login)) {
      promise = Promise.reject('token, login or id not found');
    } else {
      promise = new Promise(function (resolve, reject) {
        loadUserById()
          .then(function (data) {
            console.log('loadUserById success');
            saveUser(data.data);
            console.log(data);
            resolve(data.data);
          }, function (err) {
            console.log('loadUserById error');
            console.log(err);
            reject(err);
          });

      });
    }
    return promise;
  }

  prototype.loadCurrent = loadCurrent;
  prototype.login = login;
  prototype.logout = logout;

  return prototype;

}])
;
