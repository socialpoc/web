'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('LogoutCtrl', ['$scope', 'AuthenticationFactory', '$location', 'PageFactory',
  function ($scope, Auth, $location, Page) {

    Page.setDescription('Logout Here');
    Page.setTitle('Logout');

    $scope.logout = function () {
      console.log('logout');
      Auth.logout()
        .then(
          $scope.onFullFilledLogout('logout success'),
          function (err) {
            Page.addError('logout error',err);
            $scope.$apply();
          });
    };
  }])
;
