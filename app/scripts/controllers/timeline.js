'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:TimelineCtrl
 * @description
 * # TimelineCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('TimelineCtrl', ['$scope', 'PageFactory', 'ApiFactory', function ($scope, Page, Api) {
    Page.setDescription('This is Your timeline page. (The Walls or your Friends)');
    Page.setTitle('My Timeline');

    $scope.messages = [];

    function loadWalls() {
      Api.load('walls')
        .then(function (data) {
          angular.forEach(data,  function (wall) {
            angular.forEach($scope.user.friends, function (friend) {
              if (friend.wall.id === wall.id) {
                console.log('wall is one of %s\'s friends (%s)', $scope.user.username, wall.people.username);
                console.log(wall.messages);
                angular.forEach(wall.messages,  function (message) {
                  message.wall = wall;
                });
                $scope.messages.push.apply($scope.messages, wall.messages);
              }
            });
          });
          Page.addSuccess('load timeline success');
          $scope.$apply();
        }, function (err) {
          Page.addError('load timeline error',err);
          $scope.$apply();
        });

    }

    $scope.onUserLoad(loadWalls);
  }])
;
