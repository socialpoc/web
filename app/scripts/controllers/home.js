'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:HomeCtrl
 * @description
 * # MainCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('HomeCtrl', ['$scope', 'PageFactory', function ($scope, Page) {
    Page.setDescription('This is a social POC web app.');
    Page.setTitle('Home');
  }])
;
