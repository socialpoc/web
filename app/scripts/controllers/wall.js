'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:WallCtrl
 * @description
 * # WallCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('WallCtrl', ['$scope', 'PageFactory', 'ApiFactory', function ($scope, Page, Api) {
    $scope.wall = {};
    Page.setDescription('This is Your Wall page.');
    Page.setTitle('My Wall');

    function loadWall() {
      Api.load('walls', $scope.user.wall.id)
        .then(function (data) {
          console.log('load wall');
          $scope.wall = data;
          console.log($scope.wall);
          $scope.$apply();
        }, function (err) {
          Page.addError('load wall error',err);
          $scope.$apply();
        });

    }

    $scope.onUserLoad(loadWall);

  }])
;
