'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:FriendsCtrl
 * @description
 * # FriendsCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('FriendsCtrl', ['$scope', 'PageFactory', 'ApiFactory', function ($scope, Page, Api) {
    Page.setDescription('Show and add your Friends bellow.');
    Page.setTitle('My Friends');
    $scope.others = [];
    $scope.newFriendsIds = [];

    function extractUserByIdFrom(id, arr) {
      for (let idx = 0; idx < arr.length; idx++) {
        let user = arr[idx];
        if (user.id === id) {
          return arr.splice(idx, 1)[0];
        }
      }
    }

    function populateOthers(data) {
      angular.forEach(data, function (item) {
        console.log('loop on item %s(%s)', item.username, item.id);
        for (let i = 0; i < $scope.user.friends.length; i++) {
          let friend = $scope.user.friends[i];
          console.log('subloop on friend %s(%s)', friend.username, friend.id);
          console.log(friend);
          if (friend.id === item.id || $scope.user.id === item.id) {
            break;
          }
          if (i === $scope.user.friends.length-1) {
            $scope.others.push(item);
          }
        }
      });
    }

    function loadOtherUsers() {
      Api.load('people')
        .then(function (data) {
          populateOthers(data);
          console.log('friends.ctrl.others');
          console.log($scope.others);
          Page.addSuccess('peoples loaded successfully');
          $scope.$apply();
        }, function (err) {
          Page.addError('fail to load peoples', err);
          $scope.$apply();
        });

    }

    $scope.delFriend = function (idx) {
      console.log('deleting friend %s', idx);
      let friend = $scope.user.friends.splice(idx, 1)[0];
      console.log(friend);
      let config = {
        friends: Api.getIrisFrom('people', $scope.user.friends)
      };
      Api.save('people', config, $scope.user.id)
        .then(function () {
          $scope.others.push(friend);
          Page.addSuccess('delete friends success');
          $scope.$apply();
        }, function (err) {
          Page.addError('delete friends failed', err);
          $scope.user.friends.push(friend);
          $scope.$apply();
        });
    };
    $scope.addFriends = function () {
      let config = {
        friends: Api.getIrisFrom('people', $scope.user.friends)
      };
      angular.forEach($scope.newFriendsIds, function (id) {
        config.friends.push(Api.getIriFrom('people', id));
      });
      Api.save('people', config, $scope.user.id)
        .then(function () {
          angular.forEach($scope.newFriendsIds, function (id) {
            $scope.user.friends.push(
              extractUserByIdFrom(id, $scope.others)
            );
          });
          $scope.newFriendsIds = [];
          Page.addSuccess('add friends success');
          $scope.$apply();
        }, function (err) {
          Page.addError('add friends failed', err);
          $scope.$apply();
        });
    };

    $scope.isValidFormAddFriends = function () {
      return $scope.newFriendsIds.length > 0;
    };

    $scope.onUserLoad(loadOtherUsers);
  }])
;
