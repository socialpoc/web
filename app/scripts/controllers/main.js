'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('MainCtrl', ['$scope', 'AuthenticationFactory', '$location', 'UtilsFactory', 'PageFactory',
  function ($scope, Auth, $location, Utils, Page) {
    $scope.me = {login: 'There'};
    $scope.user = {};
    $scope.Page = Page;

    $scope.onUserLoad = function (callback) {
      if ($scope.user.id === undefined) {
        console.log('onUserLoad wait userLoad');
        $scope.$on('userLoaded', callback);
      } else {
        console.log('onUserLoad user already loaded');
        callback();
      }
    };

    $scope.onLogin = function(value) {
      $scope.user = value;
      $scope.me.login = $scope.user.username;
      $scope.Page.setAuthenticated(true);
      if ($location.path() === '/login') {
        $location.url('/');
      }
      $scope.$apply();
      $scope.$broadcast('userLoaded');
      console.log(value);
    };

    $scope.onFullFilledLogin = function(message) {
      return function (value) {
        Page.addSuccess(message);
        $scope.onLogin(value);
      };
    };

    $scope.onLogout = function() {
      $scope.user = {};
      $scope.me.login = 'There';
      $scope.Page.setAuthenticated(false);
      $location.url('/login');
      $scope.$apply();
    };

    $scope.onFullFilledLogout = function(message) {
      return function () {
        Page.addSuccess(message);
        $scope.onLogout();
      };
    };

    Auth.loadCurrent()
      .then($scope.onFullFilledLogin('loadCurrent success'),
        function (err) {
          Page.addError('loadCurrent error, redirect to /login', err);
          $location.url('/login');
          $scope.$apply();
        });

  }])
;
