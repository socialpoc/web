'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('LoginCtrl', ['$scope', 'PageFactory', 'UtilsFactory', 'AuthenticationFactory',
  function ($scope, Page, Utils, Auth) {

    Page.setDescription('Login Here');
    Page.setTitle('Login');


    $scope.login = function () {
      Auth.login($scope.user.login, $scope.user.password)
        .then(
          $scope.onFullFilledLogin('login success'),
          function (err) {
            Page.addError('login error',err);
            $scope.$apply();
          });
    };
    $scope.isValidFormLogin = function () {
      return !Utils.isBlankString($scope.user.login) && !Utils.isBlankString($scope.user.password);
    };

  }])
;
