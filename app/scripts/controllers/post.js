'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:PostCtrl
 * @description
 * # PostCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('PostCtrl', ['$scope', 'PageFactory', 'ApiFactory',
     function ($scope, Page, Api) {
    Page.setDescription('Post new message here !');
    Page.setTitle('New Post');
    $scope.friends = [];
    $scope.postContent = '';

    function generateFriend(id, wall, wallName) {
      return {
        id: id,
        wall: wall,
        wallName: wallName
      };
    }
    function loadWalls() {
      $scope.friends.push(
        generateFriend(
          $scope.user.id,
          $scope.user.wall,
          'My Wall'
        )
      );
      angular.forEach($scope.user.friends,  function (friend) {
        $scope.friends.push(
          generateFriend(
            friend.id,
            friend.wall,
            friend.username+' Wall'
          )
        );
      });
      $scope.wallId = $scope.user.wall.id;
    }

    $scope.onUserLoad(loadWalls);
    $scope.post = function () {
      Api.save('messages',{
        wall: '/api/walls/'+$scope.wallId,
        people: '/api/people/'+$scope.user.id,
        content: $scope.postContent
      })
        .then(function () {
          Page.addSuccess('post message success');
          $scope.postContent = '';
          $scope.$apply();
        }, function (err) {
          Page.addError('post message error', err);
          $scope.$apply();
        });
    };

    $scope.isValidFormPost = function() {
      return $scope.postContent.length < 4096 && $scope.postContent.length > 0;

    };

  }])
;
