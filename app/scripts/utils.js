'use strict';

function Utils(angular) {
  this.isBlankString = function(string) {
    return angular.isUndefined(string) || string === null || !angular.isString(string) || string === '';
  };
}

angular.module('webApp')
  .factory('UtilsFactory', function() {
    return new Utils(angular);
  })
;
