# web

This project is based on angularJs 1.7 and generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

Sepcs can be found at
https://gist.github.com/marmotz/a4b74bb372c4a18b629a022ec8fbb775

## Requirements

You need nodejs version 10, npm & grunt 1.3.2 to run this project.

## Build & development

Run `npm install`
Run `node_modules/bower/bin/bower install`
Run `grunt --force` for building dependencies
and `grunt serve --force` for preview.

## Nonfiguration

If your API do nor run on http://localhost:8000/api, you can modify app/scripts/app.js apiUrl constant

## Navigation

Once your server is running & successfully connected to the api,
You can

* log you on http://localhost:9000/#/login (create an account or use an existing one)
** existing accounts are : sylvain, arnaud, sandrine, cedric, foobar (with password : 'password')
* go to friends view and add some friends (http://localhost:9000/#/friends)
* go to New Post and post message (on your wall or on one of your friends) - http://localhost:9000/#/post
* show your timeline or your wall (http://localhost:9000/#/timeline - http://localhost:9000/#/wall)

## Testing

Running `grunt test` will run the unit tests with karma.

Note : there is no tests
