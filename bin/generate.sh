#!/bin/bash

ROUTES="login logout friends post wall timeline"

TEMPLATE_CONTENT=$"<div class=\"alert alert-warning\" role=\"alert\">\n"
TEMPLATE_CONTENT=$"${TEMPLATE_CONTENT}    Do what needed in controller: {{ controllerName }}"
TEMPLATE_CONTENT=$"${TEMPLATE_CONTENT} and it's template (ROUTE.html)\n</div>"

for route in $ROUTES ; do
    Route="`echo ${route:0:1} | tr [[:lower:]] [[:upper:]]`${route:1}"
    echo -e "$TEMPLATE_CONTENT" \
        | sed "s/ROUTE/$route/g" \
        > app/views/$route.html
    cat app/scripts/controllers/main.js \
        | sed "s/Main/$Route/g;s/main/$route/g" \
        > app/scripts/controllers/$route.js
done
